import urllib.request
from os import getenv

from flask import Flask, request

from petal_classifier import PetalClassifier

app = Flask(__name__)

classifier = PetalClassifier()


@app.route('/train', methods=['POST'])
def train():
    iris_data = urllib.request.urlopen(
        "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
    )
    iris_data = [i.decode("utf-8")[:-1] for i in iris_data if i.decode("utf-8")[:-1]]  # split the data

    X = [[float(j) for j in i.split(",")[:-1]]
         for i
         in iris_data
         ]
    y = [i.split(",")[-1] for i in iris_data]

    accuracy = classifier.train(X, y)
    return {"status": "Training Success", "info": "Accuracy: " + str(accuracy) + "%"}


@app.route('/predict')
def predict():
    data = request.get_json()
    data = [data] if type(data[0]) != list else data
    return {"result": list(classifier.predict(data)), "status": "Predict Success"}


@app.errorhandler(Exception)
def handle_exception(e):
    return {
        "status": "error",
        "message": "ERROR: " + str(e),
    }


if __name__ == '__main__':
    if getenv('TRAIN_WHEN_START', '1') == '1':
        train()
    debug_mode = getenv('DEBUG_MODE', '1') == '1'
    http_port = int(getenv('HTTP_PORT', '8080'))
    http_host = getenv('HTTP_HOST', '0.0.0.0')
    app.run(
        debug=debug_mode,
        host=http_host,
        port=http_port
    )
