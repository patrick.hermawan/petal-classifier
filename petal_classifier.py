from sklearn import neighbors
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split


class PetalClassifier:
    def __init__(self):
        self.classifier = None

    def train(self, X, y):
        self.classifier = neighbors.KNeighborsClassifier()
        X_train, X_test, y_train, y_test = train_test_split(X, y)

        self.classifier.fit(X_train, y_train)
        accuracy = f1_score(y_test, self.classifier.predict(X_test), average="macro")

        self.classifier.fit(X, y)

        return round(accuracy*100)

    def predict(self, X):
        return self.classifier.predict(X)
