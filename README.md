# Petal Classifier


## Prerequisites

* [Anaconda](https://www.anaconda.com/download/), [Miniconda](https://conda.io/miniconda.html) (recommended), or [Python](https://www.python.org/downloads)
* (recommended) [PyCharm](https://www.jetbrains.com/pycharm/download/) or [Visual Studio Code](https://code.visualstudio.com/download)
* (recommended) [Git](https://git-scm.com/downloads) or [SourceTree](https://www.sourcetreeapp.com/) (SourceTree already includes a Git package)


## Installation

### Downloading the package

1. Download the Petal Classifier package
    * Using Git:
        1. Open Git Bash
        2. Decide which folder you want this project's folder in. I will use `D:\Git` in this example
        3. Type `cd D:/Git`
        4. Type `git clone https://gitlab.com/patrick.hermawan/petal-classifier.git`
        5. The project will be placed in `D:/Git/petal-classifier`
    * Using SourceTree:
        1. Open a new tab in SourceTree
        2. Enter `https://gitlab.com/patrick.hermawan/petal-classifier.git` in the "Source Path / URL" section
        4. Click on "Clone"

### Preparing the environment

1. Download and install [Anaconda](https://www.anaconda.com/download/) or [Miniconda](https://conda.io/miniconda.html) (recommended)
2. Open the Anaconda Prompt
3. Move to the directory of the project
3. Create a new Conda environment by typing `conda create --name petalclassifier python=3.9`
4. Install all the necessary components by typing `pip install -r requirements.txt`
5. Conda will output the environment path, save it.
    1. In Linux environment, it will be something like: `/home/<USERNAME>/miniconda3/envs/petalclassifier/`
    1. In Windows without administrator privilege, it will be something like: `C:\Users\%userprofile%\.conda\envs\petalclassifier` or `C:\ProgramData\Anaconda\envs\petalclassifier` (with administrator privilege)
6. Activate the environment by typing `conda activate petalclassifier`
7. (optional) If you installed a package (e.g using `pip install` or `conda install`), all the new dependancies could be exported as an `requirements.txt` using the following commands:
    1. `pip freeze > requirements.txt`
9. Duplicate the file `.env.template` as `.env` and change it accordingly
8. Load up the environment variables and run the program by typing `python main.py`

## Usage

### Training
In general, it is not necessary to execute training manually since training is automatically executed when the program starts.

Should a training is needed to be executed manually, you could use the application `Postman` and send a POST request to `/train` (example: `http://172.20.10.13:8080/train`)  

The example of the response is as follows

```
{
    "info": "Accuracy: 100%",
    "status": "Training Success"
}
```

### Prediction

To run prediction, you could send a `GET` request to `/predict` 
(example: `http://172.20.10.13:8080/predict`) with the following 
payload below, in the format of `Body -> raw -> JSON`. See [here](https://learning.postman.com/docs/sending-requests/requests/#sending-body-data) 
for tutorial on sending body data with Postman.

```
[[Iris 1 sepal length in cm, Iris 1 sepal width in cm, Iris 1 petal length in cm, Iris 1 petal length in cm],
[Iris 2 sepal length in cm, Iris 2 sepal width in cm, Iris 2 petal length in cm, Iris 2 petal length in cm],
etc.
]
```

For example:

```
[[6.1,3.0,4.9,1.8], [5.0,3.3,1.4,0.2]]
```

The example of the response is as follows

```
{
    "result": [
        "Iris-virginica",
        "Iris-setosa"
    ],
    "status": "Predict Success"
}
```